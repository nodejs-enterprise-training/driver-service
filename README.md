Nama Project: driver_service

Desc: API Service untuk Driver Management, dibikin from scratch (folder kosong)

1. express --no-view --git driver_service
2. cd driver_service
3. npm install
4. npm install tedious sequelize body-parser --save
5. *npm install -g sequelize-cli
6. sequelize init
7. Buka dan ubah file config/config.json..sesuaikan dengan connection string yg mau dipakai
8. sequelize db:create
9. sequelize model:create --name Driver --attributes "nama:string email:string bio:text nohp:string status:string"
10. sequelize db:migrate
11. sequelize seed:generate --name demo-driver
12. buka seeders/*.js, lalu tambahkan data dummy atau data2 lain yg diperlukan sesuai dengan model yang udah dibikin sebelumnya
13. sequelize db:seed:all
14. Buka dengan VSCode
15. Bikin route dan controller
