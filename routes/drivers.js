var express = require('express');
var router = express.Router();

const db = require("../models");
const Op = db.Sequelize.Op;
const Driver = require("../models/driver")(db.sequelize, db.Sequelize);

/* GET all drivers list */
router.get('/', function(req, res, next) {
  Driver.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
});

/* GET search driver by name */
router.get('/search/:nama', function(req, res, next) {
  const carinama = req.params.nama;
  var condition = carinama ? { nama: { [Op.like]: `%${carinama}%` } } : null;
  Driver.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
});

module.exports = router;
